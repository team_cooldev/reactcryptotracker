import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './components/common/Header.js'
import List from './components/list/List'
import NotFound from './components/notfound/NotFound'
import Detail from './components/detail/Detail'
import './index.css'

const App =() => {

    const title = 'React Crypto Tracker'

    return (
        <BrowserRouter>
            <div>
                <Header title = { title }/>
                <Switch>
                    <Route path='/' component={ List } exact />
                    <Route path='/currency/:id' component={ Detail } exact />
                    <Route component={ NotFound } />
                </Switch>
            </div>
        </BrowserRouter>
    )
}

// Class alternative
// class App extends React.Component {
//     render() {
//         return <h1>React Coin</h1>
//     }
// }

ReactDOM.render(
    <App />,
    document.getElementById('root')
)