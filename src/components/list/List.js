import React from 'react'
import { API_URL } from '../../config'
import { handleResponse } from '../../helpers'
import Loading from '../common/Loading'
import Table from './Table'
import Pagination from './Pagination'

class List extends React.Component {

    constructor() {
        super()

        this.state = {
            loading: false,
            currencies: [],
            error: null,
            totalPages: 0,
            page: 1,
        }

        this.handlePaginationClick = this.handlePaginationClick.bind(this)
    }

    handlePaginationClick(direction) {

        const { page } = this.state

        let nextPage = page

        // Increment nextPage if direction variable is next, otherwise decrement it
        nextPage = (direction === 'next') ? nextPage+1 : nextPage-1

        // equiv notation classique
        // if (direction === 'next') {
        //     nextPage++
        // } else {
        //     nextPage--
        // }

        this.setState({
            page: nextPage
        }, () => this.fetchCurrencies())
        // call fetchCurrencies function inside setState's callback
        // to make sure that first setState is updated

    }

    fetchCurrencies() {
        this.setState({
            loading: true
        })

        const { page } = this.state

        fetch(`${ API_URL }/cryptocurrencies?page=${ page }&perPage=10`)
        .then(handleResponse)
        .then((data) => {

            const { currencies, totalPages } = data

            this.setState({
                loading: false,
                currencies: currencies,
                totalPages: totalPages,
                // shorthand equiv
                // currencies,
                // totalPages,
            })
        // console.log('Success', data);
        })
        .catch((error) => {
            this.setState({
                loading: false,
                error: error.errorMessage
            })
        // console.log('Error', error);
        });
    }

    componentDidMount() {
        this.fetchCurrencies()
    }

    render() {

        const { loading, error, currencies, page, totalPages } = this.state

        // Render only Loading component if loading state is set to true 
        if (loading) {
            return (
                <div className='Loading-container'>
                    <Loading />
                </div>
            )
        }

        // Render only error message if error occured while fetching data 
        if (error) {
            return <div className='error'>{error}</div>
        }

        return (
            <div>
                <Table 
                    currencies = {currencies}
                />

                <Pagination 
                    page= { page }
                    totalPages = { totalPages } 
                    handlePaginationClick = { this.handlePaginationClick }   
                />
            </div>
        )    
    }
}

export default List