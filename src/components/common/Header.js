import React from 'react'
import { Link } from 'react-router-dom'
import Search from './Search'
import './Header.css'
import logo from './logo.png'

// functionnal component
const Header = (props) => {

    const {title} = props

    return (
        <div>
            <div className='Header'>
                <Link to='/'>
                    <img className='Header-logo' src={logo} alt={logo}/>
                </Link >
                <Search />
            </div>
            <div className='Header-title'>
                <Link to='/'>
                    { title }
                </Link>
            </div>
        </div>
    )
}

export default  Header