import React from 'react'
import {withRouter} from 'react-router-dom'
import { API_URL } from '../../config'
import { handleResponse } from '../../helpers'
import Loading from './Loading'
import './Search.css'

class Search extends React.Component {

    constructor() {
        super()

        this.state = {
            searchResults: [],
            searchQuery: '',
            loading: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.renderSearchResults = this.renderSearchResults.bind(this)
        this.handleRedirect = this.handleRedirect.bind(this)
    }

    handleChange(e) {

        // Get the input name
        // const inputName = e.target.name

        const searchQuery = e.target.value

        // In case we have to get the value of different inputs
        // We retrieve the name of the input and assign it the corresponding value
        // this.setState({
        //     [inputName]: inputValue
        // })

        this.setState({
            searchQuery: searchQuery,
        })

        // In ES6 we can omit key value if it matches key name
        // this.setState({
        //     searchQuery
        // })

        // If searchQuery is not present don't send request to server
        if (!searchQuery) {
            return ''
        }

        this.setState({
            loading: true,
        })

        fetch (`${API_URL}/autocomplete?searchQuery=${searchQuery}`)
            .then(handleResponse)
            .then((result) => {
                this.setState({
                    loading: false,
                    searchResults: result,
                })
            })
            .catch((error) => {
                console.log(error)
            })

        // console.log(this.state)
    }

    handleRedirect(currencyId) {
        // clear input value and close autocomplete container
        // by clearing searchQuery and searchResult state
        this.setState({
            searchQuery: '',
            searchResults: [],
        })

        this.props.history.push(`/currency/${currencyId}`)
    }

    renderSearchResults() {
        const { searchResults, searchQuery, loading } = this.state

        if(!searchQuery) {
            return ''
        }

        return (
            (searchResults.length>0) ?
            <div className="Search-result-container">
                {searchResults.map(result => (
                    <div 
                        key={result.id}
                        className="Search-result"
                        onClick={() => this.handleRedirect(result.id)}>
                        {result.name} ({result.symbol})
                    </div>
                ))}
            </div> : (loading === false) ?
            <div className="Search-result-container">
                <div className="Search-no-result">
                    Result not found
                </div>
            </div> : null
        )
    }

    render() {

        const { loading, searchQuery } = this.state

        return(

            // Example of form using input names to get corresponding input values
            // <form>
            //     <input name='searchQuery'onChange = {this.handleChange}/>
            // </form>

            <div className='Search'>
                <span className='Search-icon'/>
                <input
                    className='Search-input'
                    type='text'
                    placeholder='Currency name'
                    value = { searchQuery }
                    onChange = {this.handleChange} />
                    
                { loading &&
                    <div className='Search-loading'>
                        <Loading width='12px' height='12px'/>
                    </div>}

                { this.renderSearchResults() }
            </div>
        )
    }
}

export default withRouter(Search)