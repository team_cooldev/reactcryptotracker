import React from 'react'
import { Link } from 'react-router-dom'
import './NotFound.css'

const NotFound = () => {
    return (
        <div className="NotFound">
            <h1 className='NotFound-title'>Ooops... The page you're looking for doesn't exist</h1>
            <Link className='NotFound-link' to='/'>Go to Home Page</Link>
        </div>
    )
}

export default NotFound