import React from 'react'

/**
 * Fetch error helper
 * @param {object} response 
 */

export const handleResponse = (response) => {
    return response.json().then(json => {
        return response.ok ? json : Promise.reject(json);
    });
}

/**
 * renderPercentChange helper
 * @param {string} percent 
 */
export const renderChangePercent = (percent) => {
    return (percent>0) ? <span className='percent-raised'>{percent}% &uarr;</span> : (percent<0) ? <span className='percent-fallen'>{percent}% &darr;</span> : <span>{percent}</span>
}